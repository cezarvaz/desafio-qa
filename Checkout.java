import java.util.ArrayList;

public class Checkout {

    private double total = 0.0;
    private double totalA = 0.0;
    private double totalB = 0.0;
    private double totalC = 0.0;
    private double totalD = 0.0;
    private int qtdA = 0;
    private int qtdB = 0;
    private int qtdC = 0;
    private int qtdD = 0;

    public double calculateTotal(ArrayList<String> cart) {
        parseItems(cart);
        calculateItemA();
        calculateItemB();
        calculateItemC();
        calculateItemD();
        total = totalA + totalB + totalC + totalD;
        return total;
    }

    private void parseItems(ArrayList<String> cart) {
        for (int i = 0; i < cart.size(); i++) {
            if (cart.get(i).toString().equals("A")) {
                qtdA++;
            }
            if (cart.get(i).toString().equals("B")) {
                qtdB++;
            }
            if (cart.get(i).toString().equals("C")) {
                qtdC++;
            }
            if (cart.get(i).toString().equals("D")) {
                qtdD++;
            }
        }
        System.out.println("Items A: " + qtdA);
        System.out.println("Items B: " + qtdB);
        System.out.println("Items C: " + qtdC);
        System.out.println("Items D: " + qtdD);
    }

    private void calculateItemA() {
        while (qtdA > 0) {
            if (qtdA >= 3) {
                totalA = totalA + 150.0;
                qtdA = qtdA - 3;
            } else {
                totalA = totalA + (qtdA * 50.0);
                break;
            }
        }
        System.out.println("\nTotal de items A é de: R$ " + totalA);
    }

    private void calculateItemB() {
        while (qtdB > 0) {
            if (qtdB >= 3) {
                totalB = totalB + 45.0;
                qtdB = qtdB - 3;
            } else {
                totalB = totalB + (qtdB * 30.0);
                break;
            }
        }
        System.out.println("Total de items B é de: R$ " + totalB);
    }

    private void calculateItemC() {
        totalC = qtdC * 20;
        System.out.println("Total de items C é de: R$ " + totalC);
    }

    private void calculateItemD() {
        totalD = qtdD * 15;
        System.out.println("Total de items D é de: R$ " + totalD);
    }
}
