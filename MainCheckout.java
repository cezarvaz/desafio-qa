import java.util.ArrayList;

public class MainCheckout {

    public static void main(String[] args) {
        System.out.println("\nItem   Preço     Preço");
        System.out.println("       Unitário  Especial");
        System.out.println("--------------------------");
        System.out.println("  A      50      3 por 150");
        System.out.println("  B      30      2 por 45");
        System.out.println("  C      20");
        System.out.println("  D      15\n");

        Checkout co = new Checkout();
        double total;
        ArrayList<String> cart = new ArrayList<String>();
        
        for (int i = 0; i <= args.length - 1; i++) {
            cart.add(args[i].toUpperCase());
        }

        total = co.calculateTotal(cart);
        System.out.println("\nO total da sua compra foi de: R$ " + total);
    }
}
