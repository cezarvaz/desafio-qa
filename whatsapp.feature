Funcionalidade: Whatsapp

Cen�rio: Enviar anexo de foto ou v�deo com legenda para um contato com uma conversa j� estabelecida
Dado que o app esteja aberto e conectado � Internet
Quando o usu�rio toca em uma conversa previamente estabelecida
E toca no �cone de anexos
E toca no �cone de Galeria
E escolhe um <Arquivo> de fotos ou v�deos
E digita uma legenda para o <Arquivo>
E toca no bot�o Enviar
Ent�o o <Arquivo> escolhido � enviado ao contato com sua respectiva legenda

Exemplos:
| Arquivo  |
| img.jpg  |
| vid.mp4 |

Cen�rio: Atualizar a lista de contatos
Dado que o app esteja aberto e conectado � Internet
Quando o usu�rio toca na aba Contatos
E toca no �cone de retic�ncias
E seleciona a op��o Atualizar
Ent�o a lista de contatos � atualizada
E a mensagem "Sua lista de contatos foi atualizada" � exibida